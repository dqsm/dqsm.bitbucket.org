var searchData=
[
  ['partitionbyorb',['PartitionByORB',['../classDataSpec.html#a500ba15c09534d2773a1f6e27eca05b5',1,'DataSpec']]],
  ['problem',['Problem',['../classProblem.html#ad9d44f0ef936fb62f0ce41dd200494ac',1,'Problem::Problem()'],['../classProblem.html#ab9a4ce3f2a026f562c5231e30389bcc1',1,'Problem::Problem(Kernel &amp;kernel, DataSpec &amp;dspec, ArgHandler &amp;arghandler, Real tau, Real alpha, Real beta, int rank)']]],
  ['process',['Process',['../classProcess.html#a9f4553eac74c657bb451f390c17d6bea',1,'Process']]]
];
