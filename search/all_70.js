var searchData=
[
  ['p',['P',['../classProcess.html#a0a87243421d7063bd9b22ddd073efe54',1,'Process']]],
  ['partitionbyorb',['PartitionByORB',['../classDataSpec.html#a500ba15c09534d2773a1f6e27eca05b5',1,'DataSpec']]],
  ['precalccylinders',['PreCalcCylinders',['../classProblem.html#abada395ffa9e20fd46c0526890c2bd3f',1,'Problem']]],
  ['problem',['Problem',['../classProblem.html',1,'Problem'],['../classProblem.html#ad9d44f0ef936fb62f0ce41dd200494ac',1,'Problem::Problem()'],['../classProblem.html#ab9a4ce3f2a026f562c5231e30389bcc1',1,'Problem::Problem(Kernel &amp;kernel, DataSpec &amp;dspec, ArgHandler &amp;arghandler, Real tau, Real alpha, Real beta, int rank)']]],
  ['problem_2ecc',['problem.cc',['../problem_8cc.html',1,'']]],
  ['problem_2eh',['problem.h',['../problem_8h.html',1,'']]],
  ['process',['Process',['../classProcess.html',1,'Process'],['../classProcess.html#a9f4553eac74c657bb451f390c17d6bea',1,'Process::Process()']]],
  ['process_2ecc',['process.cc',['../process_8cc.html',1,'']]],
  ['process_2eh',['process.h',['../process_8h.html',1,'']]]
];
