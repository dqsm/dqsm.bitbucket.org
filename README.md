Usage {#mainpage}
================

Making
------
The make process is automated and can detect the hardware platform for each of 3 implementations legacy, hybrid, and GPU. 

make clean implemenation=1 && make implementation=1 [multadd=1] [platform=1] [fourier_spheres=1] [profile=1]

[] denotes an optional make flag.

For legacy, replace "implementation" with "legacy"

For hybrid or GPU, replace "implementation" with "hybrid"

The rest of the optional flags are only supported by the hybrid implementation.

* multadd specifies the MultAdd mode in Process::MultAdd. It can be either "omp" (for multi-threading) or "USE_OPENCL" (for GPU vector multiplies)

* platform must be "bluegene" if not building on a laptop or GPU desktop. In this case the only multadd option is "omp" or blank.

* fourier_spheres takes on "fourier_spheres" only, and is not available on the GPU implentation (yet). 

* profile can be one of four: "pomp_profile", "mpi_profile", "hpm_profile", "gmon_profile" for OpenMP, MPI, HPM and gprof profiling, respectively. Note: it is recommended to only enable one profiling option at a time.

For instance, to make the hybrid implementation with multi-threading and MPI profiling on a bluegene: 

make clean hybrid=1 && make hybrid=1 omp=1 bluegene=1 mpi_profile=1

To make the legacy implementation:

Running
-------

To run, use the dqsm.sh script, with the following:

./dqsm.sh "implementation" p1 p2 p3 p4 p5

p1-p5 are only relevant if running a hybrid multi-threaded build.
* p1: number of nodes
* p2: number of processes
* p3: number of processes per node (should be p2/p1)
* p4: number of threads per process
* p5: number of iterations to run the algorithm

For instance, to make and run the multi-threaded hybrid build on a bluegene for 4 iterations, 256 nodes, one process per node, and 64 threads per process:

make clean hybrid=1 && make hybrid=1 omp=1 bluegene=1 mpi_profile=1 && ./dqsm.sh "hybrid" 256 256 1 64 4

Note: for convenience, all profile builds can be made/run using the dqsmprofile.sh script, suing the same syntax as above.

Modifying
--------
The shell script above can be tweaked, and knowledge of the parameters therein is useful.

# Mandatory parameters:
-DeltaB <deltab.bin>   : deltab data file
-mask <mask.bin>       : mask data file
-modelmap <models.bin> : model map data file
-out                   : output directory (must already exist)

# Optional parameters:
-model <s|m>                   : use (s)pherical model only, or (m)ixed models (ie 
                                 spherical and cylindrical). Default = m.
-chi <chi.bin>                 : initial chi array estimate. Default is all zeros.
-threshold <kernel threshold>  : minimum threshold for kernels. Any value below this 
                                 will be treated as zero. Default = 1e-6.
-mt <FA threshold>             : model FA threshold. A voxel with a value above this 
                                 will be modelled as a cylinder. Default = 0.2.
-rth <relative threshold>      : stopping criteria. Iterations will cease when the ratio 
                                 of the RMS of the change in solution to the RMS of the
                                 solution changes by less than this threshold. Default =
                                 1e-6.
-ath <absolute threshold>      : stopping criteria. Iterations will cease when the RMS of
                                 the change in the solution is less than this threshold.
                                 Default = 1e-16.
-maxiters <maximum iterations> : maximum number of iterations. Default = 1000.
-tau <tau>                     : The tau parameter in the dQSM cost function. Default =      
                                 0.15
-alpha <alpha>                 : The kappa parameter in the dQSM cost function. Default =
                                 0.75.
-x <x_iter*.bin>               : intermediate save state from previous runs (eg 
                                 x_iter000001.bin). Use this to continue calculations when 
                                 previous runs were prematurely stopped due to time outs.

# Outputs:

All output is saved to the time-stamped, configuration-specific output directory (parameter -out). The outputs are:

chi.bin          : final susceptibility map
out.bin, out.m   : files for loading output into matlab
x_iter000000.bin : intermediate saved states during computation
x_iter000001.bin
x_iter...
